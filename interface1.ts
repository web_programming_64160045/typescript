interface Rectangle {
    width: number;
    hight: number;

}

interface ColoredRectangle extends Rectangle {
    color: string;
}
const rectangle: Rectangle = {
    width: 20,
    hight: 10

}

console.log(rectangle)

const coloredRectangle: ColoredRectangle = {
    width: 20,
    hight: 10,
    color: "red"
}

console.log(coloredRectangle)